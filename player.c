#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "player.h"
#include "table.h"
#include "messages.h"

struct Player *createPlayer(char *name) ///construtor
{
    struct Player *temp = malloc(sizeof(struct Player));
    if(temp == NULL)
    {
        messageMallocError();
        printf("Function createPlayer\n");
        exit(1);
    }
    ///STRLEN - calcula o comprimento da string, excluindo o byte terminal nulo ('\0'). E retorna o numero de bytes da string
    temp->name = malloc(strlen(name)+1);
    if(temp->name == NULL)
    {
        messageMallocError();
        printf("Function createPlayer name\n");
        exit(1);
    }
    strcpy(temp->name, name);
    temp->victory = 0;
    temp->defeat = 0;
    temp->tie = 0;
    temp->score = 0;

    return temp;
}

void printPlayerStats(struct Player *player)    ///imprimi status do jogador
{
    printf("\nPlayer: %s | Score: %d\nVictory: %d | Tie: %d | Defeat: %d\n",player->name,player->score ,player->victory, player->tie, player->defeat);
}

void addVictory(struct Player *player)  ///adiciona vitoria
{
    player->victory++;
    player->score += 3;
}

void addDefeat(struct Player *player)   ///adiciona derrota
{
    player->defeat++;
}

void addTie(struct Player *player)  ///adiciona empate
{
    player->tie++;
    player->score++;
}

void setPlayerCaracterInTable(struct Player *player, struct Table *table)   ///marca a posicao no tabuleir, tendo como base a escolha do jogador
{
    table->position[table->playerPositionTranslation[0]][table->playerPositionTranslation[1]] = player->playerCaracter;
}

char *removeStringLastCaracterNewLine(char *nameTemp)   ///retira nova linha do final da string
{
    if ((strlen(nameTemp) > 0) && (nameTemp[strlen (nameTemp) - 1] == '\n'))
    {
        nameTemp[strlen (nameTemp) - 1] = '\0';
    }
    return nameTemp;
}

char *askPlayerName()   ///pergunta nome ao jogador
{
    unsigned int nameCaracterMax = 256;
    char *tempName = malloc(nameCaracterMax);
    if(tempName == NULL)
    {
        messageMallocError();
        printf("Funcao askPlayerName\n");
        exit(1);
    }
    printf("Digite o seu nome:");
    /**
    File Get Stream le caracteres ate o maximo definido, no programa e 256 ou quando uma nova linha e adicionada (guardando a nova linha tambem).
    Dessa forma, nao ha risco de buffer overflow, ou seja, o input passar o tamanho estabelecido na criacao da variavel.
    O ultimo argumento refere-se onde sera feita a leitura do input, no caso e do standard input.
    */
    fgets(tempName, nameCaracterMax, stdin);

    removeStringLastCaracterNewLine(tempName);

    return tempName;
}

struct PlayerRanking *createPlayerRanking(char *name, unsigned int score)   ///construtor
{
    struct PlayerRanking *temp = malloc(sizeof(struct PlayerRanking));
    if(temp == NULL)
    {
        messageMallocError();
        exit(1);
    }
    temp->name = malloc(strlen(name)+1);
    if(temp->name == NULL)
    {
        messageMallocError();
        exit(1);
    }
    strcpy(temp->name, name);
    temp->score = score;

    return temp;
}

unsigned int countPlayerRankingString(char *stringFile) ///descobre numero de jogaredos carregados na string
{
    unsigned int count = 0;
    for(int i = 0; i < strlen(stringFile); i++)
    {
        if(stringFile[i] == '\n')
        {
            count++;
        }
    }
    return count + 1;
}

void printStatusPlayerRanking (struct PlayerRanking *playerTemp)    ///imprimi nome e pontuacao individual
{
    printf("Score: %d | Player: %s\n",playerTemp->score, playerTemp->name);
}

void printPlayerRanking(struct PlayerRanking *playeRankingTemp[], unsigned int numberOfPlayers) ///imprimi nome e pontuacao de todo array
{
    for(int i = 0; i < numberOfPlayers; i++)
    {
        printStatusPlayerRanking(playeRankingTemp[i]);
    }
}

void buildPlayerRankingList(struct PlayerRanking *tempPlayerRanking[], char *stringFile)    /// constroe array de jogadores de ranking contidos na srtring
{
    unsigned int count = 0;
    unsigned int scoreTemp = 0;
    unsigned int iTemp = 0;
    char tempName[50];
    char tempScore[5];
    char *tempRemaining = NULL;
    for(int i = 0; i <= strlen(stringFile); i++)
    {
        if(stringFile[i] == '\0' || stringFile[i] == '\n')  ///Se chegar no final da linha ou no caracter pula linha
        {
            ///STRNCPY - similar a funcao strcpy, porem se pode delimitar o numero de bytes copiados com terceiro argumento
            strncpy(tempName, stringFile + iTemp, (i - iTemp)-3);   ///copia nome do jogador da string para string temporaria removendo a pontuacao
            for(int j = ((i - iTemp)- 3); j<= strlen(tempName);j++) ///remove lixo de memoria copiado, se houver
            {
                tempName[j] = '\0';
            }
            strncpy(tempScore, stringFile + (i - 3), 3);    ///copia pontuacao do jogador da string para string temporaria
            scoreTemp = strtol(tempScore, &tempRemaining, 10);  ///converte string para inteiro

            tempPlayerRanking[count] = createPlayerRanking(tempName, scoreTemp);    ///declara jogador de ranqueamento com as variaveis extraidas acima da string
            iTemp = i + 1;
            count++;
        }
    }
}

void addPlayerToPlayerRanking(struct Player *player1, struct PlayerRanking *playerRankingsEasy[], unsigned int *numberplayersEasy, struct PlayerRanking *playerRankingsMedium[], unsigned int *numberplayersMedium,
                              struct PlayerRanking *playerRankingsHard[], unsigned int *numberplayersHard, unsigned int gameDifficulty)     ///adiciona usuario ao array de jogadores de ranqueamento
{
    switch(gameDifficulty)  ///verifica dificuldade
    {
        case 1: ///easy
            playerRankingsEasy[*numberplayersEasy] = createPlayerRanking(player1->name, player1->score);    ///declara jogador de ranqueamento
            *numberplayersEasy = *numberplayersEasy + 1;    ///avanca variavel de controle
            rankingListSort(playerRankingsEasy, *numberplayersEasy);    ///organiza array por maior pontuacao
            break;
        case 2: ///medium
            playerRankingsMedium[*numberplayersMedium] = createPlayerRanking(player1->name, player1->score);
            *numberplayersMedium = *numberplayersMedium + 1;
            rankingListSort(playerRankingsMedium, *numberplayersMedium);
            break;
        case 3: ///hard
            playerRankingsHard[*numberplayersHard] = createPlayerRanking(player1->name, player1->score);
            *numberplayersHard = *numberplayersHard + 1;
            rankingListSort(playerRankingsHard, *numberplayersHard);
            break;
    }

}

void rankingListSort(struct PlayerRanking *playerRankingTemp[], unsigned int numberplayers) ///organiza array por maior pontuacao
{
    int count = numberplayers - 1;
    for(int i = 0; i < count; i++)
    {
        if(playerRankingTemp[i]->score < playerRankingTemp[i+1]->score) ///se pontuacao for menor que a proxima pontuacao, troca posicao no array dos jogadores
        {
            char tempName[50] = {0};
            strcpy(tempName, playerRankingTemp[i]->name);
            int tempScore = playerRankingTemp[i]->score;

            strcpy(playerRankingTemp[i]->name, playerRankingTemp[i+1]->name);
            playerRankingTemp[i]->score = playerRankingTemp[i+1]->score;

            strcpy(playerRankingTemp[i+1]->name, tempName);
            playerRankingTemp[i+1]->score = tempScore;

            i = -1;
        }
    }
}
