#ifndef HEADER_PLAYER
#define HEADER_PLAYER
/**
Arquivo de cabecalho do modulo do jogador
Cuida de todas as funcoes relacionadas ao jogador e sua estrutura
*/
struct Table;

struct Player
{
    char *name;
    unsigned int victory;
    unsigned int defeat;
    unsigned int tie;
    char playerCaracter;
    unsigned int score;
};

struct PlayerRanking
{
    char *name;
    unsigned int score;
};

/*
struct PlayerRankingList
{
    struct PlayerRanking *playerRanking;
    unsigned int numberOfPlayers;
};
*/

struct Player *createPlayer(char *name); ///construtor

void printPlayerStats(struct Player *player);   ///imprimi atributos do jogador

void addVictory(struct Player *player); ///adiciona vitoria ao jogador

void addDefeat(struct Player *player);  ///adiciona derrota ao jogador

void addTie(struct Player *player); ///adiciona empate ao jogador

void setPlayerCaracterInTable(struct Player *player, struct Table *table);///marca a posicao no tabuleir, tendo como base a escolha do jogador

char *removeStringLastCaracterNewLine(char *nameTemp);  ///retira nova linha do final da string

char *askPlayerName();  ///pergunta nome ao jogador

struct PlayerRanking *createPlayerRanking(char *name, unsigned int score);  ///construtor

unsigned int countPlayerRankingString(char *stringFile);    ///descobre numero de jogaredos carregados na string

void printStatusPlayerRanking (struct PlayerRanking *playerTemp);   ///imprimi nome e pontuacao individual

void printPlayerRanking(struct PlayerRanking *playeRankingTemp[], unsigned int numberOfPlayers);    ///imprimi nome e pontuacao de todo array

void buildPlayerRankingList(struct PlayerRanking *tempPlayerRanking[], char *stringFile);   /// constroe array de jogadores de ranking contidos na srtring

void addPlayerToPlayerRanking(struct Player *player1, struct PlayerRanking *playerRankingsEasy[], unsigned int *numberplayersEasy, struct PlayerRanking *playerRankingsMedium[],
                              unsigned int *numberplayersMedium,struct PlayerRanking *playerRankingsHard[], unsigned int *numberplayersHard, unsigned int gameDifficulty);  ///adiciona usuario ao array de jogadores de ranqueamento

void rankingListSort(struct PlayerRanking *playerRankingTemp[], unsigned int numberplayers);    ///organiza array por maior pontuacao

#endif
