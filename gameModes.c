#include <stdio.h>
#include <stdlib.h>
#include "table.h"
#include "player.h"
#include "messages.h"
#include "tableManager.h"
#include "gameModes.h"
#include "automaticPlayer.h"
#include "readAndWrite.h"

struct Player *newGame(unsigned int isDebug, unsigned int gameMode, unsigned int gameDifficulty, unsigned int isRanked)  ///inicia novo jogo
{
    unsigned int jogarNovamente = 2;

    struct Player *player1 = createPlayer(askPlayerName()); ///inicializa e declara jogador 1
    player1->playerCaracter = 'X';

    struct Player *player2 = createPlayer(askPlayerName()); ///inicializa  e declara jogador 2 ou AI no modo de 1 jogador
    player2->playerCaracter = 'O';

    struct Table *table = createTable();    ///inicializa e declara o tabuleiro
    struct Player *currentPlayer = NULL;    ///inicializa jogador temporario

    while(jogarNovamente == 2)  ///la�o de encerramento de jogo
    {
        resetTable(table);  ///reinicia valores do tabuleiro
        chooseRandomStartPlayer(&currentPlayer, player1, player2);  ///escolhe aleatoriamente jogador para iniciar a partida. A partir desse momento, jogador temporario alterna entre jogadores

        while((checkEndGame(table) && checkTie(table))) ///verifica condicao de final de jogo,
        {
            system("cls");  ///limpa console
            drawTable(table);   ///desenha tabuleiro

            if(gameMode == 1 || gameMode == 3 || gameMode == 4)  ///verifica modo de jogo 1 player ou AI depuracao ou Ranqueado
            {
                if(currentPlayer->playerCaracter == 'O')    ///joga AI
                {
                    bestPossibleMove(table, currentPlayer, player1, player2, isDebug,gameDifficulty);   ///funcao que escolhe melhor posicao para AI
                }
                else if(currentPlayer->playerCaracter == 'X')   ///joga jogador
                {
                    do
                    {
                        messageAskforInput(currentPlayer);
                        unsigned int inputNumber = askForInput(1, 9);   ///pede input para o jogador
                        fflush(stdin);  ///limpa buffer do output
                        inputTranslation(inputNumber, table);   ///traduz o input do jogador para a matriz

                    }while(checkOccupiedSpace(table));  ///verifica se a escolha do usuario esta ocupada
                }
            }
            else if(gameMode == 2)  ///modo de 2 jogadores
            {
                do
                {
                    messageAskforInput(currentPlayer);
                    unsigned int inputNumber = askForInput(1, 9);   ///pede input para o jogador
                    fflush(stdin);  ///limpa buffer do output
                    inputTranslation(inputNumber, table);   ///traduz o input do jogador para a matriz

                }while(checkOccupiedSpace(table));  ///verifica se a escolha do usuario esta ocupada
            }

            setPlayerCaracterInTable(currentPlayer, table); ///marca posicao que o jogador escolheu no tabuleiro

            switchPlayer(currentPlayer, player1, player2);  ///alterna jogador
        }

        system("cls");  ///limpa console

        drawTable(table);   ///desenha tabuleiro

        checkWinner(table,currentPlayer, player1, player2); ///verifica ganhador

        printPlayerStats(player1);  ///mostra status do jogador 1


        if(gameMode == 4)   ///modo ranqueado. Jogador continua jogando ate perder
        {
            messageRankedGame1();
            getchar();
            fflush(stdin);
            messageRankedGame2();
            if(player1->defeat >= 1)    ///se jogador humano perder, encerra o ciclo de partidas
            {
               return player1;
            }

        }
        else
        {
            printPlayerStats(player2);  ///mostra status do jogador 2
            messagePlayAgain();
            jogarNovamente = askForInput(1, 2); ///pergunta ao usuario se ira jogar novamente
            fflush(stdin);
        }

    }
    return 0;
}

void printPlayerRankingByDifficulty(struct PlayerRanking *playerRankingsEasy[], unsigned int numberplayersEasy, struct PlayerRanking *playerRankingsMedium[], unsigned int numberplayersMedium,
                                    struct PlayerRanking *playerRankingsHard[], unsigned int numberplayersHard, unsigned int gameDifficulty)    ///imprimi nome e pontuacao de todas as dificuldades
{
    switch(gameDifficulty)
    {
        case 0:
            messageShowRanking3();
            printPlayerRanking(playerRankingsHard, numberplayersHard);
            messageShowRanking2();
            printPlayerRanking(playerRankingsMedium, numberplayersMedium);
            messageShowRanking1();
            printPlayerRanking(playerRankingsEasy, numberplayersEasy);
            break;
        case 1:
            messageShowRanking1();
            printPlayerRanking(playerRankingsEasy, numberplayersEasy);
            break;
        case 2:
            messageShowRanking2();
            printPlayerRanking(playerRankingsMedium, numberplayersMedium);
            break;
        case 3:
            messageShowRanking3();
            printPlayerRanking(playerRankingsHard, numberplayersHard);
            break;
    }
}
