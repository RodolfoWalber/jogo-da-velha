#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "messages.h"

struct Table *createTable() ///construtor
{
    struct Table *temp = malloc(sizeof(struct Table));
    if(temp == NULL)
    {
        messageMallocError();
        printf("Function createTable\n");
        exit(1);
    }

    temp->position[0] = malloc(sizeof(char)*3);
    strcpy(temp->position[0], "123");
    temp->position[1] = malloc(sizeof(char)*3);
    strcpy(temp->position[1], "456");
    temp->position[2] = malloc(sizeof(char)*3);
    strcpy(temp->position[2], "789");

    temp->rounds = 0;

    return temp;
}

void drawTable(struct Table *table)    /// Imprimi na tela o tabuleiro
{
	for(int i = 0; i < 3; i++)  /// Loop responsavel pela impress�o do tabuleiro
    {
        for(int j = 0; j < 3; j++)
        {
            printf("%c ",table->position[i][j]);
        }
        printf("\n");
    }
}

void addRound (struct Table *table) ///adiciona rodadas
{
    table->rounds++;
}

void resetTable(struct Table *table)    ///reseta posicoes para nova partida
{
    strcpy(table->position[0], "123");
    strcpy(table->position[1], "456");
    strcpy(table->position[2], "789");
}
