#ifndef HEADER_GAMEMODES
#define HEADER_GAMEMODES
/**
Arquivo de cabecalho do modulo de modos de jogo
Cuida da funcao relacionada ao inicio de um novo jogo e mostra a lista do ranqueamento de jogadores
*/
struct PlayerRanking;

//struct PlayerRankingList;

struct Player *newGame(unsigned int isDebug, unsigned int gameMode, unsigned int gameDifficulty, unsigned int isRanked);    /// Inicia novo jogo

void printPlayerRankingByDifficulty(struct PlayerRanking *playerRankingsEasy[], unsigned int numberplayersEasy, struct PlayerRanking *playerRankingsMedium[], unsigned int numberplayersMedium,
                                    struct PlayerRanking *playerRankingsHard[], unsigned int numberplayersHard, unsigned int gameDifficulty);   ///imprimi nome e pontuacao de todas as dificuldades

#endif

