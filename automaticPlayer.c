#include <stdio.h>
#include <stdlib.h>
#include "table.h"
#include "player.h"
#include "messages.h"
#include "tableManager.h"
#include "automaticPlayer.h"

/**
Funcao recursiva que busca final de jogo e atribui pontuacao de acordo, retornando-a. Cada vez que e chamada, ela muda de jogador. Se marcou o tabuleiro como X, sua proxima jogada sera como O e assim por diante
ate chegar no final do jogo daquela posicao inicial especifica.
*/
int automaticPLayer(struct Table *table,struct Table *tempTable, struct Player *currentPlayer, struct Player *player1, struct Player *player2, int isDebug)
{
    int lookUpTableScore = 0;
    messageAutomaticPlayer1(isDebug);

    if(checkTie(tempTable) == 0 || checkEndGame(tempTable) == 0) ///condicao de saida da recursao - fim de jogo
    {
        if((checkTie(tempTable) == 0) && (checkEndGame(tempTable) == 1))
        {
            lookUpTableScore = 0;  ///empate
            messageAutomaticPlayer2(isDebug, lookUpTableScore);
        }
        else if(currentPlayer->playerCaracter == 'O')
        {
            lookUpTableScore = 1;  ///bot ganha
            messageAutomaticPlayer2(isDebug, lookUpTableScore);
        }
        else if(currentPlayer->playerCaracter == 'X')
        {
            lookUpTableScore = -1; ///jogador ganha
            messageAutomaticPlayer2(isDebug, lookUpTableScore);
        }
        return lookUpTableScore; ///retorna a pontuacao
    }

    switchPlayer(currentPlayer, player1, player2);  ///troca jogador
    if(currentPlayer->playerCaracter == 'O')    ///movimento AI
    {
        messageAutomaticPlayerAi1(isDebug);
        int bestScore = -1024;
        for(int i = 0; i < 3; i++) ///coluna
        {
            for(int j = 0; j < 3; j++)  ///linha
            {
                if(tempTable->position[i][j] != 'X' && tempTable->position[i][j] != 'O')
                {
                    currentPlayer->playerCaracter = 'O';    ///caracter do bot 'O'
                    char tempChar = tempTable->position[i][j];  ///copia posicao livre
                    messageAutomaticPlayerAi2(isDebug, tempChar);

                    tempTable->playerPositionTranslation[0] = i;    ///salva posicao atual no tabuleiro
                    tempTable->playerPositionTranslation[1] = j;    ///salva posicao atual no tabuleiro
                    setPlayerCaracterInTable(currentPlayer, tempTable); ///marca posicao atual no tabuleiro
                    messageAutomaticPlayerAi3(isDebug);
                    if(isDebug)
                    {
                        drawTable(tempTable);
                    }

                    int score = automaticPLayer(table, tempTable, currentPlayer, player1, player2, isDebug);    ///chama bot para avaliar proxima posicao

                    messageAutomaticPlayerAi4(isDebug);
                    switchPlayer(currentPlayer, player1, player2);  ///troca jogador
                    tempTable->position[i][j] = tempChar;   ///desmarca a posicao marcada anteriormente

                    if(score > bestScore)   ///guarda melhor pontuacao para bot
                    {
                        bestScore = score;
                        messageAutomaticPlayerAi5(isDebug, bestScore);
                    }
                }
            }
        }
        return bestScore;
    }
    else    ///movimento jogador
    {
        messageAutomaticPlayerHuman1(isDebug);
        int bestScore = 1024;
        for(int i = 0; i < 3; i++) ///coluna
        {
            for(int j = 0; j < 3; j++)  ///linha
            {
                if(tempTable->position[i][j] != 'X' && tempTable->position[i][j] != 'O')
                {
                    currentPlayer->playerCaracter = 'X';    ///caracter do jogador 'X'
                    char tempChar = tempTable->position[i][j];  ///copia posicao livre
                    messageAutomaticPlayerHuman2(isDebug, tempChar);

                    tempTable->playerPositionTranslation[0] = i;    ///salva posicao atual no tabuleiro
                    tempTable->playerPositionTranslation[1] = j;    ///salva posicao atual no tabuleiro
                    setPlayerCaracterInTable(currentPlayer, tempTable); ///marca posicao atual no tabuleiro
                    messageAutomaticPlayerHuman3(isDebug);
                    if(isDebug)
                    {
                        drawTable(tempTable);
                    }

                    int score = automaticPLayer(table,tempTable, currentPlayer, player1, player2, isDebug); ///chama bot para avaliar proxima posicao

                    messageAutomaticPlayerHuman4(isDebug);
                    tempTable->position[i][j] = tempChar;   ///desmarca a posicao marcada anteriormente
                    switchPlayer(currentPlayer, player1, player2);  ///troca jogador

                    if(score < bestScore)   ///guarda melhor pontuacao para jogador
                    {
                        bestScore = score;
                        messageAutomaticPlayerHuman5(isDebug,bestScore);
                    }
                }
            }
        }
        return bestScore;   ///retorna melhor pontuacao
    }
}
/**
Funcao que escolhe a melhor jogada baseada na pontuacao (vitoria +1, empate 0, derrota -1), olhando para todas as opcoes disponiveis no tabuleiro.
Para cada espaco vazio ela chama a funcao recursiva "automaticPLayer" que olha as jogadas possiveis e condicoes de final desse espaco.
No final a melhor pontuacao e guardada juntamente com a posicao no tabuleiro
*/
void bestPossibleMove(struct Table *table, struct Player *currentPlayer, struct Player *player1, struct Player *player2, int isDebug, int gameDifficulty)
{
    int i2 = 0;
    int j2 = 0;
    struct Table *tempTable = createTable();    ///cria tabuleiro
    messageBestPossibleMove1(isDebug);  ///habilita msg de depuracao no console
    if(isDebug)
    {
        drawTable(table);
    }

    struct Player *tempCurrentPlayer = createPlayer(player2->name); ///cria jogador temporario
    *tempCurrentPlayer = *currentPlayer;    ///copia conteudo do jogador atual para o temporario
    int bestScore = -1024;  ///variavel de controle
    if(gameDifficulty == 1)
    {
        bestScore = 1024;
    }
    for(int i = 0; i < 3; i++) ///coluna
    {
        for(int j = 0; j < 3; j++)  ///linha
        {
            for(int i = 0; i < 3; i++) ///Tabuleiro Copia - coluna
            {
                for(int j = 0; j < 3; j++)  ///Tabuleiro Copia - linha
                {
                    tempTable->position[i][j] = table->position[i][j];  ///laco que copia a tabela original para uma copia
                }
            }

            if(tempTable->position[i][j] != 'X' && tempTable->position[i][j] != 'O')    ///verifica se posicao j� foi marcada
            {
                tempCurrentPlayer->playerCaracter = 'O';    ///caracter do bot 'O'
                char tempChar = tempTable->position[i][j];  ///copia posicao livre
                messageBestPossibleMove2(isDebug,tempChar);

                tempTable->playerPositionTranslation[0] = i;    ///salva posicao atual no tabuleiro
                tempTable->playerPositionTranslation[1] = j;    ///salva posicao atual no tabuleiro
                setPlayerCaracterInTable(tempCurrentPlayer, tempTable); ///marca posicao atual no tabuleiro
                int score = 0;

                score = automaticPLayer(table, tempTable, tempCurrentPlayer, player1, player2, isDebug);    ///funcao recursiva que retorna pontuacao de final de jogo

                messageBestPossibleMove3(isDebug, i, j);
                tempTable->position[i][j] = tempChar;   ///desmarca a posicao marcada anteriormente

                if(gameDifficulty == 1) ///muda logica para diminuir dificuldade
                {
                    if(score < bestScore)
                    {
                        bestScore = score;
                        i2 = i;
                        j2 = j;
                        messageBestPossibleMove4(isDebug,i2, j2, bestScore);
                    }
                }
                else
                {
                    if(score > bestScore)
                    {
                        bestScore = score;  ///salva a melhor pontuacao
                        i2 = i;
                        j2 = j;
                        messageBestPossibleMove4(isDebug,i2, j2, bestScore);
                    }
                }
            }
        }
    }
    messageBestPossibleMove1(isDebug);
    if(isDebug)
    {
        drawTable(table);
    }
    unsigned int number = diceD2();
    if(gameDifficulty == 2 && number == 0)  ///se dificuldade normal, 50% de chance de o bot jogar aleatoriamente
    {
        randomMove(table);
    }
    else
    {
        messageBestPossibleMove5(isDebug, i2, j2);
        table->playerPositionTranslation[0] = i2;   ///marca a melhor jogada
        table->playerPositionTranslation[1] = j2;   ///marca a melhor jogada
    }
    free(tempCurrentPlayer);    ///libera ponteiro temporario
    free(tempTable);    ///libera ponteiro temporario
}

void randomMove(struct Table *table)    ///escolhe aleatoriamente a posicao que ainda esta disponivel no tabuleiro
{
    while(1)
    {
        table->playerPositionTranslation[0] = diceD3();
        table->playerPositionTranslation[1] = diceD3();
        if(table->position[table->playerPositionTranslation[0]][table->playerPositionTranslation[1]] != 'X' &&
           table->position[table->playerPositionTranslation[0]][table->playerPositionTranslation[1]] != 'O')
        {
            break;
        }
    }




}
