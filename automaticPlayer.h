#ifndef HEADER_AUTOMATICPLAYER
#define HEADER_AUTOMATICPLAYER
/**
Arquivo de cabecalho do modulo do bot
Cuida de todas as funcoes relacionadas ao bot
*/

int automaticPLayer(struct Table *table,struct Table *tempTable, struct Player *currentPlayer, struct Player *player1, struct Player *player2, int isDebug);    ///funcao recursiva que retorna as jogadas possiveis tanto do player 1 como 2

void bestPossibleMove(struct Table *table, struct Player *currentPlayer, struct Player *player1, struct Player *player2, int isDebug, int gameDifficulty);  ///funcao que avalia as melhores jogadas

void randomMove(struct Table *table);   ///escolhe aleatoriamente a posicao que ainda esta disponivel no tabuleiro

#endif
