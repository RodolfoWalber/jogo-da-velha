#ifndef HEADER_TABLE
#define HEADER_TABLE
/**
Arquivo de cabecalho do modulo do tabuleiro
Cuida de todas as funcoes relacionadas ao tabuleiro e sua estrutura
*/

struct Table
{
    char *position[3];
    unsigned int rounds;
    unsigned int playerPositionTranslation[2];
};

struct Table *createTable();    ///construtor

void drawTable(struct Table *table);    ///desenha tabuleiro

void addRound (struct Table *table);    ///adiciona rodada

void resetTable(struct Table *table);   ///reinicia posicoes no tabuleiro

#endif
