#ifndef HEADER_MESSAGES
#define HEADER_MESSAGES
/**
Arquivo de cabecalho do modulo de menssagens
Cuida de todos os textos do programa
*/

struct Table;
struct Player;

void messageCongratulations(char *currentPlayer);

void messageTie();

void messageMallocError();

void messageFileError();

void messageAskforInput(struct Player *currentPlayer);

void messageInvalidInput();

void messageWelcome();

void messagePlayAgain();

void messageGameMode();

void messageChooseDifficulty();

void messageExitGame();

void messageShowRanking1();

void messageShowRanking2();

void messageShowRanking3();

void messageRankedGame1();

void messageRankedGame2();

void messageBestPossibleMove1(int isDebug);

void messageBestPossibleMove2(int isDebug, char position);

void messageBestPossibleMove3(int isDebug, int i, int j);

void messageBestPossibleMove4(int isDebug, int i2, int j2, int bestScore);

void messageBestPossibleMove5(int isDebug, int i2, int j2);

void messageAutomaticPlayer1(int isDebug);

void messageAutomaticPlayer2(int isDebug, int lookUpTableScore);

void messageAutomaticPlayerAi1(int isDebug);

void messageAutomaticPlayerAi2(int isDebug, char position);

void messageAutomaticPlayerAi3(int isDebug);

void messageAutomaticPlayerAi4(int isDebug);

void messageAutomaticPlayerAi5(int isDebug, int bestScore);

void messageAutomaticPlayerHuman1(int isDebug);

void messageAutomaticPlayerHuman2(int isDebug, char position);

void messageAutomaticPlayerHuman3(int isDebug);

void messageAutomaticPlayerHuman4(int isDebug);

void messageAutomaticPlayerHuman5(int isDebug, int bestScore);

#endif
