#include <stdio.h>
#include <stdlib.h>
#include "messages.h"
#include "table.h"
#include "player.h"



void messageCongratulations(char *currentPlayer)  ///funcao que parabeniza o ganhador
{
	printf("Congratulations! %s is the Winner!", currentPlayer);
}

void messageTie()   ///funcao que imprime na tela  caso a partida nao tenha nenhum ganhador
{
	printf("Tie :|\n");
}

void messageMallocError()   ///funcao de erro na alocacao dinamica
{
    printf("Falha na alocacao de memoria\n");
}

void messageFileError() ///funcao de erro na abertura do arquivo
{
    printf("Arquivo Nao Aberto\n");
}

void messageAskforInput(struct Player *currentPlayer)   ///funcao que orienta usuario a escolher uma posicao no tabuleiro
{
    printf("%s, please enter the desired number to mark in the board: ", currentPlayer->name);
}

void messageInvalidInput()  ///funcao que avisa input de caracter errado
{
    printf("Invalid input. Please, try again.\n");
}

void messageWelcome()   ///funcao de boas vindas
{
    printf("Welcome to Tic Tac Toe\n");
}

void messagePlayAgain() ///funcao que pergunta se havera outro jogo
{
    printf("\nPlay again?\n");
    printf("1 - No\n2 - Yes!\n");
}

void messageGameMode()  ///orienta usuario na escolha de modo de jogo
{
    printf("Choose a game mode:\n");
    printf("1 - 1 player game\n2 - 2 player game\n3 - AI Depuracao\n4 - Ranked Game\n5 - Show Ranking\n");
}

void messageChooseDifficulty()  ///orienta usuario na escolha de dificuldade do jogo
{
    printf("Choose the difficulty:\n");
    printf("1 - easy\n2 - medium\n3 - hard\n");
}

void messageExitGame()  ///orienta usuario na escolha de troca de menus
{
    printf("Do you want to exit the game or change the mode?\n");
    printf("1 - Exit game\n2 - Back to Menu\n");
}

void messageShowRanking1()  ///mostra dificuldade do ranking
{
    printf("\n-------Easy Difficulty Score-------\n");
}

void messageShowRanking2()
{
    printf("\n-------Medium Difficulty Score-------\n");
}

void messageShowRanking3()
{
    printf("\n-------Hard Difficulty Score-------\n");
}

void messageRankedGame1()
{
    printf("\nGood Game!\n");
    printf("Press Any Key To Continue.\n");
}

void messageRankedGame2()
{
    printf("\nGame Over!\n");
}

void messageBestPossibleMove1(int isDebug)
{
    if(isDebug)
    {
        printf("In BPM\n");
        printf("BPM - Table ORIGINAL\n");
    }
}

void messageBestPossibleMove2(int isDebug, char position)
{
    if(isDebug)
    {
        printf("BPM - Empty Space: %c\n",position);
    }
}

void messageBestPossibleMove3(int isDebug, int i, int j)
{
    if(isDebug)
    {
        printf("BPM - Returning\n");
        printf("BPM i: %d\n",i);
        printf("BPM j: %d\n",j);
    }
}

void messageBestPossibleMove4(int isDebug, int i2, int j2, int bestScore)
{
    if(isDebug)
    {
        printf("BPM - bestScore\n");
        printf("BPM - bestScore: %d\n", bestScore);
        printf("///i2: %d\n///j2: %d\n",i2,j2);
    }
}

void messageBestPossibleMove5(int isDebug, int i2, int j2)
{
    if(isDebug)
    {
        printf("BPM - Atribution Table Position\n");
        printf("/////////////i2: %d\n/////////////j2: %d\n",i2,j2);
    }
}

void messageAutomaticPlayer1(int isDebug)
{
    if(isDebug)
    {
        printf("In AP\n");
    }
}

void messageAutomaticPlayer2(int isDebug, int lookUpTableScore)
{
    if(isDebug)
    {
        printf("\nTERMINAL STATE - Score: %d\n\n", lookUpTableScore);
    }
}

void messageAutomaticPlayerAi1(int isDebug)
{
    if(isDebug)
    {
        printf("In AP AI\n");;
    }
}

void messageAutomaticPlayerAi2(int isDebug, char position)
{
    if(isDebug)
    {
        printf("AP AI - Empty Space: %c\n",position);
    }
}

void messageAutomaticPlayerAi3(int isDebug)
{
    if(isDebug)
    {
        printf("AP AI - Table Copy\n");
    }
}

void messageAutomaticPlayerAi4(int isDebug)
{
    if(isDebug)
    {
        printf("AP AI - Returning\n");
    }
}

void messageAutomaticPlayerAi5(int isDebug, int bestScore)
{
    if(isDebug)
    {
        //printf("AP AI - bestScore\n");
        printf("AP AI - bestScore: %d\n", bestScore);
    }
}

void messageAutomaticPlayerHuman1(int isDebug)
{
    if(isDebug)
    {
        printf("In AP Human\n");;
    }
}

void messageAutomaticPlayerHuman2(int isDebug, char position)
{
    if(isDebug)
    {
        printf("AP Human - Empty Space: %c\n",position);
    }
}

void messageAutomaticPlayerHuman3(int isDebug)
{
    if(isDebug)
    {
        printf("AP Human - Table Copy\n");
    }
}

void messageAutomaticPlayerHuman4(int isDebug)
{
    if(isDebug)
    {
        printf("AP Human - Returning\n");
    }
}

void messageAutomaticPlayerHuman5(int isDebug, int bestScore)
{
    if(isDebug)
    {
        //printf("AP Human - bestScore\n");
        printf("AP Human - bestScore: %d\n", bestScore);
    }
}


