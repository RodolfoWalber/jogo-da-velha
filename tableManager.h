#ifndef HEADER_TABLEMANAGER
#define HEADER_TABLEMANAGER
/**
Arquivo de cabecalho do modulo do gerenciamento do tabuleiro
Cuida de todas as funcoes que gerenciam e coordenam o tabuleiro e os jogadores
*/

int checkOccupiedSpace(struct Table *table);    /// ///verifica se o espaco escolhido pelo usuario esta livre

void inputTranslation(unsigned int inputNumber, struct Table *table);   /// Funcao que traduz o imput do usuario para a matriz do tabuleiro

unsigned int askForInput(unsigned int minValue, unsigned int maxValue); /// Pede input do jogador e valida caracter

int checkEndGame(struct Table *table);  ///verifica sequencia de X ou O nas linhas, colunas e diagonais

void chooseRandomStartPlayer(struct Player **currentPlayer, struct Player *player1, struct Player *player2);    ///escolhe aleatoriamente jogador

void checkWinner(struct Table *table, struct Player *currentPlayer, struct Player *player1, struct Player *player2);    ///verifica ganhador e altera variaveis dos jogadores

int diceD2();   ///escolha aleatoria entre 0 e 1

int diceD3();   ///escolha aleatoria entre 0 e 2

int checkTie(struct Table *table);  ///verifica se todos os espacos estao ocupados no tabuleiro

void switchPlayer(struct Player *currentPlayer, struct Player *player1, struct Player *player2);    ///alterna entre jogadores

#endif
