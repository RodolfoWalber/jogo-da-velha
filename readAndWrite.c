#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "table.h"
#include "player.h"
#include "messages.h"
#include "tableManager.h"
#include "automaticPlayer.h"
#include "readAndWrite.h"

FILE *openFileRead(char addressFile[])  ///carrega arquivo em modo de leitura apontado pelo endereco e vincula ao stream
{
    FILE *tempFile = NULL;

    tempFile = fopen(addressFile, "rb");
    if(tempFile == NULL)
    {
        messageFileError();
        exit(1);
    }

    return tempFile;
}

FILE *openFileWrite(char addressFile[]) ///carrega arquivo em modo de escrita apontado pelo endereco e vincula ao stream
{
    FILE *tempFile = NULL;

    tempFile = fopen(addressFile, "w+b");
    if(tempFile == NULL)
    {
        messageFileError();
        exit(1);
    }

    return tempFile;
}

void closeFile(FILE *tempFile)  ///desvincula o arquivo ao stream e limpa o buffer interno
{
    fclose(tempFile);
}

char *FileStreamToString(FILE *textFile)    ///copia stream para string
{
    char *stringTemp = NULL;
    long int lengthFile = 0;
    ///FSEEK - reposiciona a posicao do indicador de leitura/escrita do stream
    fseek(textFile, 0, SEEK_END);   ///move o indicador do stream para o final do arquivo (EOF)
    ///FTELL - descobre a posicao atual do indicador do stream e retorna o offset em bytes
    lengthFile = ftell(textFile);   ///descobre tamanho do stream
    fseek(textFile, 0, SEEK_SET);   ///volta indicador do stream para inicio do arquivo
    stringTemp = malloc(lengthFile + 1);    ///aloca string com tamanho da stream + null byte
    if(stringTemp == NULL)
    {
        messageMallocError();
        exit(1);
    }
    fread(stringTemp,1,lengthFile,textFile);    ///le um bloco de dados do stream do tamanho especificado pelo lengthFile e armazena na string
    stringTemp[lengthFile] = '\0';  ///acrescenta byte nulo no final da string
    return stringTemp;
}

char *formatString(struct PlayerRanking *tempPlayerRanking[], unsigned int numberplayers)   ///formata string para escrita no arquivo txt
{
    char *stringFile = NULL;
    char *stringTemp = malloc(sizeof(char)*3);
    int stringLenght = numberplayers + (numberplayers * 3) + 1; ///inicia variavel contabilizando caracteres de nova linha, espacos e pontuacao
    for(int i = 0; i < numberplayers;i ++)
    {
        stringLenght += strlen(tempPlayerRanking[i]->name); ///acrescenta o tamanho dos nomes dos jogadores de ranqueamento
    }
    stringFile = malloc(stringLenght);  ///aloca memoria com tamanho dos nomes e pontuacao dos jogadores
    stringFile[0] = '\0';   ///configura string para nulo

    for(int i = 0; i < numberplayers; i++)
    {
/**
STRCAT - concatena strings - anexa uma copia dastring original na string destino. O byte nulo � substitido pelo primeiro caracter da string original e o byte nulo e adicionado ao final da nova string
formada na string destino
*/
        strcat(stringFile, tempPlayerRanking[i]->name); ///adiciona nome do jogador a string
        strcat(stringFile, " ");    ///adiciona espaco a string
        ///SPRINTF - armazena output em buffer de char (stringTemp)
        sprintf(stringTemp, "%d", tempPlayerRanking[i]->score); ///converte int para char
        strcat(stringFile, stringTemp); ///adiciona pontuacao a string
        if(i != numberplayers - 1)
        {
            strcat(stringFile, "\n");   ///adiciona nova linha a string
        }
    }

    return stringFile;
}

void writeFile(FILE *tempFile, char *stringFile)    ///escreve n elementos, de tamanho especificado, da string para a stream
{
    fwrite(stringFile,1,strlen(stringFile),tempFile);
}

void writePlayerRankingList(FILE *gameModesScoreFileEasy, FILE *gameModesScoreFileMedium, FILE *gameModesScoreFileHard, struct PlayerRanking *playerRankingsEasy[],
                            unsigned int numberplayersEasy, struct PlayerRanking *playerRankingsMedium[], unsigned int numberplayersMedium, struct PlayerRanking *playerRankingsHard[],
                            unsigned int numberplayersHard, unsigned int gameDifficulty, char addresScoreEasy[], char addresScoreMedium[], char addresScoreHard[])  /// organiza escrita de array jogadores de ranqueamento por dificuldade
{
    char *stringTemp;
    switch(gameDifficulty)  ///verifica dificuldade
    {
        case 1: ///easy
            gameModesScoreFileEasy = openFileWrite(addresScoreEasy);    ///carrega arquivo em modo de escrita apontado pelo endereco e vincula ao stream
            stringTemp = formatString(playerRankingsEasy, numberplayersEasy);   ///formata string para escrita no arquivo txt
            writeFile(gameModesScoreFileEasy, stringTemp);  ///escreve string ao file stream
            closeFile(gameModesScoreFileEasy);  ///desvincula o arquivo ao stream e limpa o buffer interno
            break;
        case 2: ///medium
            gameModesScoreFileMedium = openFileWrite(addresScoreMedium);
            stringTemp = formatString(playerRankingsMedium, numberplayersMedium);
            writeFile(gameModesScoreFileMedium, stringTemp);
            closeFile(gameModesScoreFileMedium);
            break;
        case 3: ///hard
            gameModesScoreFileHard = openFileWrite(addresScoreHard);
            stringTemp = formatString(playerRankingsHard, numberplayersHard);
            writeFile(gameModesScoreFileHard, stringTemp);
            closeFile(gameModesScoreFileHard);
            break;
    }
}
