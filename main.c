/**
REFERENCIA:
-https://stackoverflow.com/questions/228617/how-do-i-clear-the-console-in-both-windows-and-linux-using-c
-https://stackoverflow.com/questions/11656532/returning-an-array-using-c
-https://www.geeksforgeeks.org/difference-while1-while0-c-language/
-https://stackoverflow.com/questions/8633436/how-to-interpret-structure-without-definition
-https://stackoverflow.com/questions/51985705/warning-declaration-of-will-not-be-visible-outside-this-function-wvisib
-https://stackoverflow.com/questions/1719662/warning-struct-user-data-s-declared-inside-parameter-list
-https://stackoverflow.com/questions/26583717/how-to-scanf-only-integer
-https://linux.die.net/man/3/strlen
-https://www.educative.io/edpresso/what-is-strtol-in-c
-http://man7.org/linux/man-pages/man3/strtol.3.html
-https://stackoverflow.com/questions/39938648/copy-one-pointer-content-to-another
-https://en.wikipedia.org/wiki/Minimax
-https://www.geeksforgeeks.org/recursion/
-https://linux.die.net/man/3/fseek
-https://linux.die.net/man/3/ftell
-https://stackoverflow.com/questions/174531/how-to-read-the-content-of-a-file-to-a-string-in-c
-https://stackoverflow.com/questions/5242524/converting-int-to-string-in-c
-https://linux.die.net/man/3/strcat
-https://stackoverflow.com/questions/35178520/how-to-read-parse-input-in-c-the-faq
-https://stackoverflow.com/questions/6205195/given-a-starting-and-ending-indices-how-can-i-copy-part-of-a-string-in-c
-https://www.quora.com/How-can-I-delete-last-item-in-array-in-C
-https://linux.die.net/man/3/strncpy
-https://stackoverflow.com/questions/47429855/copying-string-using-strncpy-cause-garbage-value-in-output
-https://stackoverflow.com/questions/1453876/why-does-strncpy-not-null-terminate
-https://stackoverflow.com/questions/6185821/how-do-we-check-if-a-pointer-is-null-pointer
-https://stackoverflow.com/questions/14015967/in-c-how-to-get-number-of-items-in-array-of-pointers-to-char-strings


TODO:


WHISHLIST:
-commando clear console para o window e sistema posix
-reavaliar uso da variavel rounds
-implementar a AI usando estrutura de arvore binaria
-criar estrutura de controle dos jogadores ranqueados
-unificar arquivos .txts
-converter .txt para .bin
-refatorar funcoes de leitura e escrita
-integrar estrutura playerRanking com player ?


BUGS:
-modo ranqueado, nao tem como perder para a AI e voltar par o menu
-leitura de arquivo de nome de jogador e pontuacao incorreta se a pontuacao for menor que 10

BUGS:


*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include "messages.h"
#include "tableManager.h"
#include "gameModes.h"
#include "readAndWrite.h"
#include "player.h"

int main()
{
    messageWelcome();
    srand(time(NULL));  ///inicializa gerador randomico atrelado ao horario
    unsigned int exitGame = 2;

    char addresScoreEasy[] = "rankingEasy.txt"; ///guarda endereco de arquivo
    char addresScoreMedium[] = "rankingMedium.txt";
    char addresScoreHard[] = "rankingHard.txt";
    FILE *gameModesScoreFileEasy = openFileRead(addresScoreEasy);   ///abre o arquivo e vincula ao stream
    FILE *gameModesScoreFileMedium = openFileRead(addresScoreMedium);
    FILE *gameModesScoreFileHard = openFileRead(addresScoreHard);
    char *stringFileEasy = FileStreamToString(gameModesScoreFileEasy);  ///transfere stream para string
    char *stringFileMedium = FileStreamToString(gameModesScoreFileMedium);
    char *stringFileHard = FileStreamToString(gameModesScoreFileHard);
    closeFile(gameModesScoreFileEasy);  ///fecha arquivo
    closeFile(gameModesScoreFileMedium);
    closeFile(gameModesScoreFileHard);
    unsigned int numberplayersEasy = countPlayerRankingString(stringFileEasy);  ///descobre numero de jogaredos carregados na string
    unsigned int numberplayersMedium = countPlayerRankingString(stringFileMedium);
    unsigned int numberplayersHard = countPlayerRankingString(stringFileHard);
    struct PlayerRanking *playerRankingsEasy[numberplayersEasy];    ///inicializa array de jogadores
    struct PlayerRanking *playerRankingsMedium[numberplayersMedium];
    struct PlayerRanking *playerRankingsHard[numberplayersHard];
    buildPlayerRankingList(playerRankingsEasy, stringFileEasy); ///declara jogadores e pontuacao
    buildPlayerRankingList(playerRankingsMedium, stringFileMedium);
    buildPlayerRankingList(playerRankingsHard, stringFileHard);

    while(exitGame == 2)
    {
        unsigned int gameMode = 0;
        unsigned int isDebug = 0;
        unsigned int gameDifficulty = 0;
        unsigned int isRanked = 0;
        messageGameMode();
        gameMode = askForInput(1, 5);   ///usuario escolhe modo de jogo
        fflush(stdin);  ///limpa buffer do output

        switch(gameMode)
        {
            case 1: ///modo 1 player
                messageChooseDifficulty();
                gameDifficulty = askForInput(1, 3); ///usuario escolhe dificuldade da AI
                fflush(stdin);
                newGame(isDebug,gameMode,gameDifficulty,isRanked);   ///inicia novo jogo
                break;
            case 2: ///modo 2 players
                newGame(isDebug,gameMode,gameDifficulty,isRanked);   ///inicia novo jogo
                break;
            case 3: ///AI depuracao
                isDebug = 1;
                newGame(isDebug,gameMode,gameDifficulty,isRanked);   ///inicia novo jogo
                break;
            case 4: ///modo ranqueado
                isRanked = 1;
                messageChooseDifficulty();
                gameDifficulty = askForInput(1, 3); ///usuario escolhe dificuldade da AI
                fflush(stdin);
                struct Player *playerTemp = newGame(isDebug,gameMode,gameDifficulty,isRanked);
                addPlayerToPlayerRanking(playerTemp, playerRankingsEasy, &numberplayersEasy, playerRankingsMedium, &numberplayersMedium, playerRankingsHard, &numberplayersHard, gameDifficulty);
                printPlayerRankingByDifficulty(playerRankingsEasy, numberplayersEasy, playerRankingsMedium, numberplayersMedium, playerRankingsHard, numberplayersHard, gameDifficulty);
                writePlayerRankingList(gameModesScoreFileEasy,gameModesScoreFileMedium, gameModesScoreFileHard, playerRankingsEasy, numberplayersEasy, playerRankingsMedium, numberplayersMedium,
                                       playerRankingsHard, numberplayersHard, gameDifficulty, addresScoreEasy, addresScoreMedium, addresScoreHard);
                break;
            case 5: ///consulta lista de ranqueamento
                printPlayerRankingByDifficulty(playerRankingsEasy, numberplayersEasy, playerRankingsMedium, numberplayersMedium, playerRankingsHard, numberplayersHard, 0);
                break;
        }
        messageExitGame();
        exitGame = askForInput(1, 2);   ///usuario escolhe se encerra o programa
        fflush(stdin);
    }
    return 0;
}
