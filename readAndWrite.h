#ifndef HEADER_READANDWRITE
#define HEADER_READANDWRITE
/**
Arquivo de cabecalho do modulo de escrita e leitura de arquivos
Cuida de todas as funcoes relacionadas ao bot
*/

FILE *openFileRead(char addressFile[]); ///carrega arquivo em modo de leitura apontado pelo endereco e vincula ao stream

FILE *openFileWrite(char addressFile[]);    ///carrega arquivo em modo de escrita apontado pelo endereco e vincula ao stream

void closeFile(FILE *tempFile); ///desvincula o arquivo ao stream e limpa o buffer interno

char *FileStreamToString(FILE *textFile);   ///copia stream para string

char *formatString(struct PlayerRanking *tempPlayerRanking[], unsigned int numberplayers);  ///formata string para escrita no arquivo txt

void writeFile(FILE *tempFile, char *stringFile);   ///escreve n elementos, de tamanho especificado, da string para a stream

void writePlayerRankingList(FILE *gameModesScoreFileEasy, FILE *gameModesScoreFileMedium, FILE *gameModesScoreFileHard, struct PlayerRanking *playerRankingsEasy[],
                            unsigned int numberplayersEasy, struct PlayerRanking *playerRankingsMedium[], unsigned int numberplayersMedium, struct PlayerRanking *playerRankingsHard[],
                            unsigned int numberplayersHard, unsigned int gameDifficulty, char addresScoreEasy[], char addresScoreMedium[], char addresScoreHard[]); /// organiza escrita de array jogadores de ranqueamento por dificuldade

#endif
