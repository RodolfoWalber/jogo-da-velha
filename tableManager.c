#include <stdio.h>
#include <stdlib.h>
#include "table.h"
#include "player.h"
#include "messages.h"
#include "tableManager.h"

int checkOccupiedSpace(struct Table *table) ///verifica se o espaco escolhido pelo usuario esta livre
{
	if (table->position[table->playerPositionTranslation[0]][table->playerPositionTranslation[1]] == 'X' ||
        table->position[table->playerPositionTranslation[0]][table->playerPositionTranslation[1]] == 'O')   /// Caso o campo seja 'X' ou 'O'...
    {
		return 1;
	}

	return 0;
}

void inputTranslation(unsigned int inputNumber, struct Table *table)  /// Funcao que traduz o input do usuario para a matriz do tabuleiro
{
	switch(inputNumber) /// Switch recebe valor da variavel chamada na funcao "AskForInput"
	{
		case 1:
			table->playerPositionTranslation[0] = 0;
			table->playerPositionTranslation[1] = 0;
		break;

		case 2:
			table->playerPositionTranslation[0] = 0;
			table->playerPositionTranslation[1] = 1;
		break;

		case 3:
			table->playerPositionTranslation[0] = 0;
			table->playerPositionTranslation[1] = 2;
		break;

		case 4:
			table->playerPositionTranslation[0] = 1;
			table->playerPositionTranslation[1] = 0;
		break;

		case 5:
			table->playerPositionTranslation[0] = 1;
			table->playerPositionTranslation[1] = 1;
		break;

		case 6:
			table->playerPositionTranslation[0] = 1;
			table->playerPositionTranslation[1] = 2;
		break;

		case 7:
			table->playerPositionTranslation[0] = 2;
			table->playerPositionTranslation[1] = 0;
		break;

		case 8:
			table->playerPositionTranslation[0] = 2;
			table->playerPositionTranslation[1] = 1;
		break;

		case 9:
			table->playerPositionTranslation[0] = 2;
			table->playerPositionTranslation[1] = 2;
		break;
	}
}

unsigned int askForInput(unsigned int minValue, unsigned int maxValue) /// Pede input do jogador e valida caracter
{
    unsigned long int tempNumber = 0;
    char *stringRemaining = NULL;
    char userInput[50];

    while(1) ///protecao contra caracteres invalidos, tanto letras como numeros
    {
        fgets(userInput, sizeof(userInput), stdin);
        /**
        Strtol - converte string para inteiro longo. Ela tambem ignora os espacos em branco iniciais ate o inteiro, se houver.
        O input pode conter um mix de inteiros e caracteres, se a primeira parte for inteiro(s), ele e atribuido a variavel do tipo long int e
        o restante da string e colocada em um ponteiro de char. Caso nao haja inteiro na string, retorna 0;
        */
        tempNumber = strtol(userInput, &stringRemaining, 10);

        if(tempNumber >= minValue && tempNumber <= maxValue)
        {
            break;
        }
        messageInvalidInput();
    }
    //free(stringRemaining);

	return tempNumber;
}


int checkEndGame(struct Table *table)  ///verifica sequencia de X ou O nas linhas, colunas e diagonais
{
	if ((table->position[0][0] == table->position[1][1] && table->position[1][1] == table->position[2][2]) ||
        (table->position[0][2] == table->position[1][1] && table->position[1][1] == table->position[2][0]))   /// Verifica diagonais por caractere igual
    {
		return 0;
	}

    for(int i = 0; i < 3; i++)
    {
        if ((table->position[i][0] == table->position[i][1] && table->position[i][1] == table->position[i][2]) ||
            (table->position[0][i] == table->position[1][i] && table->position[1][i] == table->position[2][i])) ///Verifica linhas e colunas por caracter igual
        {
            return 0;
        }
    }
	return 1;
}

int diceD2()    ///escolha aleatoria entre 0 e 1
{
    unsigned int temp = rand()%2;
    return temp;
}

int diceD3()    ///escolha aleatoria entre 0 e 2
{
    unsigned int temp = rand()%3;
    return temp;
}

void chooseRandomStartPlayer(struct Player **currentPlayer, struct Player *player1, struct Player *player2) ///escolhe aleatoriamente jogador
{
    char *nameTemp = NULL;
    unsigned int temp = diceD2();
    if(temp == 0)
    {
        nameTemp = malloc((sizeof(player1->name)+1));
        *currentPlayer = createPlayer(nameTemp);
        **currentPlayer = *player1;
    }
    else if(temp == 1)
    {
        nameTemp = malloc((sizeof(player2->name)+1));
        *currentPlayer = createPlayer(nameTemp);
        **currentPlayer = *player2;
    }
    free(nameTemp);
}

void checkWinner(struct Table *table, struct Player *currentPlayer, struct Player *player1, struct Player *player2) ///verifica ganhador e altera variaveis dos jogadores
{
    if((checkTie(table) == 0) && (checkEndGame(table) == 1))    ///empate
    {
        messageTie();
        addTie(player1);
        addTie(player2);
    }
    else
    {
        if(currentPlayer->playerCaracter == 'X')    ///vitoria do X
        {
            messageCongratulations(player2->name);
            addVictory(player2);
            addDefeat(player1);
        }
        else if(currentPlayer->playerCaracter == 'O')   /// vitoria do O
        {
            messageCongratulations(player1->name);
            addVictory(player1);
            addDefeat(player2);
        }
    }
}

int checkTie(struct Table *table)   ///verifica se todos os espacos estao ocupados no tabuleiro
{
    int temp = 0;
    for(int i = 0; i < 3; i++) ///coluna
    {
        for(int j = 0; j < 3; j++)  ///linha
        {
            if(table->position[i][j] == 'X' || table->position[i][j] == 'O')
            {
                temp++;
            }
        }
    }
    if(temp == 9)
    {
        return 0;
    }
    return 1;
}

void switchPlayer(struct Player *currentPlayer, struct Player *player1, struct Player *player2) ///alterna entre jogadores
{
    if(currentPlayer->playerCaracter == 'X')
    {
        *currentPlayer = *player2;
    }
    else if(currentPlayer->playerCaracter == 'O')
    {
        *currentPlayer = *player1;
    }

}
